# Desafio StartDB (em desenvolvimento)

Desafio proposto para a pratica de conceitos do desenvolvimento web full stack durante o período de encubação do programa de estagio StartDB.

## Autores

- [@liliantavarez](https://github.com/liliantavarez)
- [@quynhorodrigues](https://gitlab.com/quynhorodrigues)

## 🚀 Tecnologias
- 💾 Mongoose
- ⚡ Express 
- ⚛️ React
- 🍃 Node

## 🔥 Instalação e execução
1. Instale o [Node](https://nodejs.org/en/)
2. Faça um clone desse repositório;
3. Entre na pasta `cd desafio-startdb api`;
4. Rode `npm install`  para instalar as dependências;
5. Use `npm run start` para rodar a api;
6. Entre na pasta `cd desafio-startdb client`;
7. Rode `npm install`  para instalar as dependências;
8. Use `npm run start` para rodar a aplicação;
