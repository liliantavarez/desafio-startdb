import {Ave} from "../entities/ave.js";
import {AveModel} from "./aveModel.js";
import {AvesRepository} from "./avesRepositorie.js";

export class AveRepositoryMongoose implements AvesRepository {
	inserirAve(ave: Ave): Promise<Ave> {
		return AveModel.create(ave);
	}

	buscaTodas(): Promise<Ave[]> {
		const consulta = AveModel.find();
		return consulta.exec();

	}

	buscaComFiltro(filtro: string): Promise<Ave[]> {
		const consulta = AveModel.find({
			$or: [
				{nomePtBr: new RegExp(filtro, "i")},
				{nomeCientifico: new RegExp(filtro, "i")},
				{familia: new RegExp(filtro, "i")},
			],
		});
		return consulta.exec();
	}
}
