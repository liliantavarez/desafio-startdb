import {Ave} from "../entities/ave.js";
import {Schema, model} from "mongoose";

const AveSchema = new Schema<Ave>({
	nomePtBr: {type: String, required: true, maxlength: 40},
	nomeCientifico: {type: String, required: true, maxlength: 40},
	altura: {type: String, required: true, maxlength: 7},
	familia: {type: String, required: true, maxlength: 40},
});

export const AveModel = model<Ave>("Ave", AveSchema, "aves");
