import {Anotacao} from "../entities/anotacao.js";
import {model, Schema, SchemaTypes} from "mongoose";

const AnotacaoSchema = new Schema<Anotacao>({
	data: {type: Date},
	horario: {type: Date},
	local: {type: String},
	aveIdentificada: [{type: SchemaTypes.ObjectId, ref: "Ave"}],
});

export const AveModel = model<Anotacao>(
	"Anotacao",
	AnotacaoSchema,
	"anotacoes",
);
