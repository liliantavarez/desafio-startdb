import {Ave} from "../entities/ave.js";

export interface AvesRepository {
	buscaTodas(): Promise<Ave[]> | null;
	buscaComFiltro(filtro: string): Promise<Ave[]> ;
	inserirAve(ave: Ave): Promise<Ave>;
}
