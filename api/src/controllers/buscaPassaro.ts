import {Request, Response, NextFunction} from "express";
import {Ave} from "../entities/ave.js";
import {AveRepositoryMongoose} from "../repositories/aveRepositoryMongoose.js";

/* buscar com filtro no banco de dados */
export async function controladorBuscaPassaro(
	req: Request,
	res: Response,
	next: NextFunction,
) {
	try {
		const filtro: string = req.body.filtro;
		const repositorio = new AveRepositoryMongoose();
		const resultado: Ave[] = await repositorio.buscaComFiltro(filtro);
		

		res.json(resultado);
		
	} catch (erro) {
		next();
	}
}
