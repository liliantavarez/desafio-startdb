import {Request, Response, NextFunction} from "express";
import {AveModel} from "../repositories/aveModel.js";
import {AveRepositoryMongoose} from "../repositories/aveRepositoryMongoose.js";

/* cadastrando no banco de dados */
export async function controladorCadastroPassaro(
	req: Request,
	res: Response,
	next: NextFunction,
) {
	try {
		let passaro = new AveModel(req.body);
		const repositorio = new AveRepositoryMongoose();
		const resultado = await repositorio.inserirAve(passaro);

		res.json(resultado);
	} catch (erro) {
		next();
	}
}
