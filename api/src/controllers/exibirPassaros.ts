import {Request, Response, NextFunction} from "express";
import {AveRepositoryMongoose} from "../repositories/aveRepositoryMongoose.js";

/* buscar com filtro no banco de dados */
export async function controladorExibirPassaros(
	req: Request,
	res: Response,
	next: NextFunction,
) {
	try {
		const repositorio = new AveRepositoryMongoose();
		const resultado = await repositorio.buscaTodas();
		

		res.json(resultado);
	} catch (erro) {
		next();
	}
}
