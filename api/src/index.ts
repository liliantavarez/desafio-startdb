import app from "./services/config/app.js";
import {connectDB} from "./services/config/database.js";
import {PORT} from "./services/config/config.js";

async function main() {
	await connectDB();
	app.listen(PORT);
	console.log("Server on port ", PORT);
}

main();