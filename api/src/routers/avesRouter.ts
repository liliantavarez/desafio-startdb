import express from "express";
const router = express.Router();

import {controladorExibirPassaros} from "../controllers/exibirPassaros.js";
import {controladorBuscaPassaro} from "../controllers/buscaPassaro.js";
import {controladorCadastroPassaro} from "../controllers/cadastroPassaro.js";


router.get("/", controladorExibirPassaros);
router.get("/busca", controladorBuscaPassaro);
router.post("/cadastroPassaro", controladorCadastroPassaro);


export default router;
