import { Ave } from "./ave.js";

export interface Anotacao {
  data: Date,
  horario: Date,
  local: string,
  aveIdentificada: Ave;
}
