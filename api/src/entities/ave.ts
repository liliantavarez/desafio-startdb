export interface Ave {
    nomePtBr: string,
    nomeCientifico: string,
    altura: string,
    familia: string
}
