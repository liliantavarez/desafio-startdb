import {Ave} from "../entities/ave.js";
import {AvesRepository} from "../repositories/avesRepositorie.js";
import {AveRepositoryMongoose} from "../repositories/aveRepositoryMongoose.js";
import baseDeDados from "./helpers/mongodb_helper.js";

describe("AveRepositoryMongooose", () => {
	beforeAll(async () => {
		await baseDeDados.abrir();
	});
	afterAll(async () => {
		await baseDeDados.fechar();
	});
	beforeEach(async () => {
		await baseDeDados.inicializarDados();
	});
	afterEach(async () => {
		await baseDeDados.limpar();
	});

	const repositorio: AvesRepository = new AveRepositoryMongoose();

	describe("buscaComFiltro()", () => {
		const expectativa: Ave[] = [];
		test("deve retornar um array vazio para nenhuma ave encontrada com determinado filtro", async () => {
			const filtro = "Papagaio";
			const ave = await repositorio.buscaComFiltro(filtro);
			expect(ave).toEqual(expectativa);
		});

		test("deve retornar ave da Família Mimidae", async () => {
			const filtro = "Mimidae";
			const ave = await repositorio.buscaComFiltro(filtro);
			expect(ave[0]).toBeDefined();
			expect(ave[0]?.nomePtBr).toBe("calhandra-de-três-rabos");
			expect(ave[0]?.nomeCientifico).toBe("Mimus triurus");
			expect(ave[0]?.altura).toBe("20");
			expect(ave[0]?.familia).toBe("Família Mimidae");
		});
	});
});
