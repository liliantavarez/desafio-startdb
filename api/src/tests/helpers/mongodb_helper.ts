import {MongoMemoryServer} from "mongodb-memory-server";
import mongoose from "mongoose";

class BdEmMemoria {
	private mongoServer?: MongoMemoryServer;
	private static intancia: BdEmMemoria;

	public static getInstancia() {
		if (!BdEmMemoria.intancia) {
			BdEmMemoria.intancia = new BdEmMemoria();
		}
		return BdEmMemoria.intancia;
	}

	public async abrir() {
		try {
			this.mongoServer = await MongoMemoryServer.create();
			const uri = this.mongoServer.getUri();
			await mongoose.connect(uri);
		} catch (error) {
			console.log("Falha de criação do BD em memória.");
			console.log(error);
			throw error;
		}
	}

	public async fechar() {
		try {
			await mongoose.connection.dropDatabase();
			await mongoose.disconnect();
			if (this.mongoServer) {
				await this.mongoServer.stop();
			}
		} catch (error) {
			console.log("Falha de encerramento do BD em memória.");
			console.log(error);
			throw error;
		}
	}

	public async limpar() {
		try {
			const colecoes = mongoose.connection.collections;
			for (const nomeColecao in colecoes) {
				const colecao = colecoes[nomeColecao];
				await colecao.deleteMany({});
			}
		} catch (error) {
			console.log("Falha de limpeza das coleções do BD em memória.");
			console.log(error);
			throw error;
		}
	}

	public async inicializarDados() {
		try {
			const aves = mongoose.connection.collection("aves");
			await aves.insertMany([
				{
					nomePtBr: "rolinha-picuí",
					nomeCientifico: "Columbina picui",
					altura: "15",
					familia: "Família Columbidae",
				},
				{
					nomePtBr: "calhandra-de-três-rabos",
					nomeCientifico: "Mimus triurus",
					altura: "20",
					familia: "Família Mimidae",
				},
			]);
		} catch (error) {
			console.log("Falha de inicialização das coleções do BD em memória.");
			console.log(error);
			throw error;
		}
	}
}

export default BdEmMemoria.getInstancia();
