import {useCallback, useState} from "react";
import {Passaro} from "../interfaces/Passaro";
import {PassaroService} from "../services/PassaroService";

export const usePassaro = () => {
	const [passaros, setPassaros] = useState<Passaro[]>([]);
	const passarosCadastrados = useCallback(async () => {
		const {status, data} = await PassaroService.passarosCadastrados();

		if (status !== 200) throw new Error();
		setPassaros(data);
		return passaros;
	}, [passaros]);

	const novoPassaro = useCallback(async (passaro: Passaro) => {
		const {status} = await PassaroService.cadastroPassaro(passaro);

		if (status !== 200) throw new Error();
	}, []);

	return {
		passaros,
		passarosCadastrados,
		novoPassaro,
	};
};
