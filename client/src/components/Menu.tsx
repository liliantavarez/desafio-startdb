import {useCallback, useEffect, useState} from "react";
import {Passaro} from "../interfaces/Passaro";
import NavDropdown from "react-bootstrap/NavDropdown";
import Container from "react-bootstrap/Container";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import Nav from "react-bootstrap/Nav";
import logo from "./imagens/logo.png";
import "../css/CartaoPassaro.css";
import React from "react";
import "../css/Menu.css";
import { usePassaro } from "../hooks/usePassaro";

function Menu() {
	const [filtro, setFiltro] = useState("");
	const {passarosCadastrados} = usePassaro();
	const [passaros, setPassaros] = useState<Passaro[]>([]);
	React.useEffect(() => {
		passarosCadastrados()
			.then((results: React.SetStateAction<Passaro[]>) => {
				setPassaros(results);
			})
			.catch((error: string | undefined) => {
				throw new Error(error);
			});
	}, [passarosCadastrados]);

	return (
		<>
			<Navbar expand="lg">
				<Container fluid className="App-header">
					<div className="logo-name">
						<figure>
							<img
								alt="imagem da logo do site"
								src={logo}
								className="d-inline-block align-top logo"
							/>{" "}
						</figure>
					</div>

					<Navbar.Toggle aria-controls="navbarScroll" />
					<Navbar.Collapse id="navbarScroll">
						<Nav className="menu" style={{maxHeight: "100px"}} navbarScroll>
							<Nav.Link href="/">Home</Nav.Link>
							<NavDropdown title="Cadastro" id="navbarScrollingDropdown">
								<NavDropdown.Item href="/cadastroPassaro">
									Passaro
								</NavDropdown.Item>
								<NavDropdown.Item href="#action4">Avistamento</NavDropdown.Item>
							</NavDropdown>
						</Nav>
						<Form className="d-flex">
							<Button className="btn-buscar" variant="outline">
								Busca
							</Button>
							<Form.Control
								type="search"
								aria-label="Search"
								value={filtro}
								onChange={(event) => setFiltro(event.target.value)}
							/>
						</Form>
					</Navbar.Collapse>
				</Container>
			</Navbar>

			<div className="board">
				{passaros
					.filter((val) => {
						if (filtro === "") {
							return val;
						} else if (
							val.nomePtBr.toLowerCase().includes(filtro.toLowerCase())
						) {
							return val;
						}
					})
					.map((passaro: Passaro) => (
						<div>
							<Card style={{width: "18rem"}}>
								<Card.Header>{passaro.nomeCientifico}</Card.Header>
								<Card.Body>
									<Card.Title>{passaro.nomePtBr}</Card.Title>
									<Card.Text>
										<ul>
											<li>{passaro.familia}</li>
											<li>Altura: {passaro.altura} (cm)</li>
										</ul>
									</Card.Text>
								</Card.Body>
							</Card>
						</div>
					))}
			</div>
		</>
	);
}

export default Menu;
