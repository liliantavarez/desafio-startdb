import { Container } from 'react-bootstrap';
import bemvindo from './imagens/bemvindo.png';

function rodape() {
    return (
<Container>
	<div className="bemvindo-name">
					<figure>
						<img
							alt="rodape do site"
							src={bemvindo}
							className="d-inline-block align-top logo"
						/>{" "}
					</figure>
				</div>
	</Container>

    );
    
}
export default rodape;
