import {useCallback, useState} from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {usePassaro} from "../hooks/usePassaro";
import {Passaro} from "../interfaces/Passaro";
import "../css/CadastroPassaro.css";

export function CadastroPassaro() {
	const [nomePtBr, setNome] = useState("");
	const [nomeCientifico, setNomeCientifico] = useState("");
	const [familia, setFamilia] = useState("");
	const [altura, setAltura] = useState("");
	const [validated, setValidated] = useState(false);
	const {passaros, passarosCadastrados, novoPassaro} = usePassaro();

	const handleValidated = (event: any) => {
		event.preventDefault();
		const form = event.currentTarget;

		if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		}

		setValidated(true);
	};

	const handleCadastroPassaro = useCallback(async () => {
		const dados: Passaro = {nomePtBr, nomeCientifico, familia, altura};
		console.log(dados);
		await novoPassaro(dados);
	}, [altura, familia, nomeCientifico, nomePtBr, novoPassaro]);

	return (
		<>
			<Form
				noValidate
				validated={validated}
				onSubmit={handleValidated}
				onClick={handleCadastroPassaro}
				method={"POST"}
				className="formCadastroPassaro">
				<h2>Novo Passaro</h2>
				<Form.Group className="input-form" controlId="validationCustom01">
					<Form.Control
						required
						maxLength={40}
						type="text"
						placeholder="Nome"
						value={nomePtBr}
						onChange={(event) => setNome(event.target.value)}
					/>
				</Form.Group>
				<Form.Group className="input-form">
					<Form.Control
						required
						maxLength={40}
						type="text"
						placeholder="Nome cientifico"
						value={nomeCientifico}
						onChange={(event) => setNomeCientifico(event.target.value)}
					/>
				</Form.Group>
				<Form.Group className="input-form">
					<Form.Control
						required
						maxLength={40}
						type="text"
						placeholder="Familia"
						value={familia}
						onChange={(event) => setFamilia(event.target.value)}
					/>
				</Form.Group>
				<Form.Group className="input-form">
					<Form.Control
						required
						maxLength={7}
						placeholder="Medida (cm)"
						type="text"
						value={altura}
						onChange={(event) =>
							setAltura(event.target.value.replace(/[^\d-]/g, ""))
						}
					/>
				</Form.Group>
				<Button variant="outline" className="btn-cadastrar" type="submit">
					Cadastrar
				</Button>
			</Form>
		</>
	);
}
