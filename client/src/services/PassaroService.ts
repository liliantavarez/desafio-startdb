import {Passaro} from "./../interfaces/Passaro";
import {Api} from "./../providers/index";

const passarosCadastrados = () => Api.get<Passaro[]>("/");
const cadastroPassaro = (passaro:Passaro) => Api.post<Passaro>("/cadastroPassaro", passaro);

export const PassaroService = {
	passarosCadastrados,
    cadastroPassaro
};
