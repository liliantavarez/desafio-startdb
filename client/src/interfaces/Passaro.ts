export interface Passaro {
	nomePtBr: string;
	nomeCientifico: string;
	familia: string;
	altura: string;
}
